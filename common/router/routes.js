Router.map(function() {
    this.route('index.view', {path: '/'});
    this.route('slideshow.view', {path: '/slideshow'});
    this.route('login.view', {path: '/login'});
});